{{/* default nginx configuration template */}}
{{/* Generate a configuration file based on the containers mandatory */}}
{{/* VIRTUAL_HOST environment variable and the exposed ports. If multiple */}}
{{/* ports are exposed, the first one is used, unless set with VIRTUAL_PORT */}}


{{- /*
     * Global values.  Values are stored in this map rather than in individual
     * global variables so that the values can be easily passed to embedded
     * templates.  (Go templates cannot access variables outside of their own
     * scope.)
     */}}
{{- $globals := dict }}
{{- $_ := set $globals "containers" $ }}
{{- $_ := set $globals "Env" $.Env }}
{{- $_ := set $globals "Docker" $.Docker }}
{{- $_ := set $globals "CurrentContainer" (where $globals.containers "ID" $globals.Docker.CurrentContainerID | first) }}

{{- $_ := set $globals "networks" (dict) }}
# Networks available to the container running docker-gen (which are assumed to
# match the networks available to the container running nginx):
{{- /*
     * Note: $globals.CurrentContainer may be nil in some circumstances due to
     * <https://github.com/nginx-proxy/docker-gen/issues/458>.  For more context
     * see <https://github.com/nginx-proxy/nginx-proxy/issues/2189>.
     */}}
{{- if $globals.CurrentContainer }}
    {{- range sortObjectsByKeysAsc $globals.CurrentContainer.Networks "Name" }}
        {{- $_ := set $globals.networks .Name . }}
#     {{ .Name }}
    {{- else }}
#     (none)
    {{- end }}
{{- else }}
# /!\ WARNING: Failed to find the Docker container running docker-gen.  All
#              upstream (backend) application containers will appear to be
#              unreachable.  Try removing the -only-exposed and -only-published
#              arguments to docker-gen if you pass either of those.  See
#              <https://github.com/nginx-proxy/docker-gen/issues/458>.
{{- end }}

{{- /*
     * Template used as a function to get a container's IP address.  This
     * template only outputs debug comments; the IP address is "returned" by
     * storing the value in the provided dot dict.
     *
     * The provided dot dict is expected to have the following entries:
     *   - "globals": Global values.
     *   - "container": The container's RuntimeContainer struct.
     *
     * The return value will be added to the dot dict with key "ip".
     */}}
{{- define "container_ip" }}
    {{- $ip := "" }}
    #     networks:
    {{- range sortObjectsByKeysAsc $.container.Networks "Name" }}
        {{- /*
             * TODO: Only ignore the "ingress" network for Swarm tasks (in case
             * the user is not using Swarm mode and names a network "ingress").
             */}}
        {{- if eq .Name "ingress" }}
    #         {{ .Name }} (ignored)
            {{- continue }}
        {{- end }}
        {{- if eq .Name "host" }}
            {{- /* Handle containers in host nework mode */}}
            {{- if (index $.globals.networks "host") }}
    #         both container and proxy are in host network mode, using localhost IP
                {{- $ip = "127.0.0.1" }}
                {{- continue }}
            {{- end }}
            {{- range sortObjectsByKeysAsc $.globals.CurrentContainer.Networks "Name" }}
                {{- if and . .Gateway (not .Internal) }}
    #         container is in host network mode, using {{ .Name }} gateway IP
                    {{- $ip = .Gateway }}
                    {{- break }}
                {{- end }}
            {{- end }}
            {{- if $ip }}
                {{- continue }}
            {{- end }}
        {{- end }}
        {{- if and (not (index $.globals.networks .Name)) (not $.globals.networks.host) }}
    #         {{ .Name }} (unreachable)
            {{- continue }}
        {{- end }}
        {{- /*
             * Do not emit multiple `server` directives for this container if it
             * is reachable over multiple networks.  This avoids accidentally
             * inflating the effective round-robin weight of a server due to the
             * redundant upstream addresses that nginx sees as belonging to
             * distinct servers.
             */}}
        {{- if $ip }}
    #         {{ .Name }} (ignored; reachable but redundant)
            {{- continue }}
        {{- end }}
    #         {{ .Name }} (reachable)
        {{- if and . .IP }}
            {{- $ip = .IP }}
        {{- else }}
    #             /!\ No IP for this network!
        {{- end }}
    {{- else }}
    #         (none)
    {{- end }}
    #     IP address: {{ if $ip }}{{ $ip }}{{ else }}(none usable){{ end }}
    {{- $_ := set $ "ip" $ip }}
{{- end }}

{{- /*
     * Template used as a function to get the port of the server in the given
     * container.  This template only outputs debug comments; the port is
     * "returned" by storing the value in the provided dot dict.
     *
     * The provided dot dict is expected to have the following entries:
     *   - "container": The container's RuntimeContainer struct.
     *
     * The return value will be added to the dot dict with key "port".
     */}}
{{- define "container_port" }}
    {{- /* If only 1 port exposed, use that as a default, else 80. */}}
    #     exposed ports:{{ range sortObjectsByKeysAsc $.container.Addresses "Port" }} {{ .Port }}/{{ .Proto }}{{ else }} (none){{ end }}
    {{- $default_port := when (eq (len $.container.Addresses) 1) (first $.container.Addresses).Port "80" }}
    #     default port: {{ $default_port }}
    {{- $port := or $.container.Env.VIRTUAL_PORT $default_port }}
    #     using port: {{ $port }}
    {{- $addr_obj := where $.container.Addresses "Port" $port | first }}
    {{- if and $addr_obj $addr_obj.HostPort }}
    #         /!\ WARNING: Virtual port published on host.  Clients
    #                      might be able to bypass nginx-proxy and
    #                      access the container's server directly.
    {{- end }}
    {{- $_ := set $ "port" $port }}
{{- end }}

server {
  listen 80 default_server;
  server_name _; # This is just an invalid value which will never trigger on a real hostname.
  error_log /proc/self/fd/2;
  access_log /proc/self/fd/1;
  return 503;
}

{{ range $host, $containers := groupByMulti $ "Env.VIRTUAL_HOST" "," }}
upstream {{ $host }} {

{{- $servers := 0 }}

{{ range $container := $containers }}

  {{ if eq $container.Env.DEPLOYMENT_COLOR (env "DEPLOYMENT_COLOR") }}

    {{ if $container.State.Health.Status }}
      {{ if ne $container.State.Health.Status "healthy" }}
        {{ continue }}
      {{ end }}
    {{ end }}

    # Container: {{ $container.Name }}
        {{- $args := dict "globals" $globals "container" $container }}
        {{- template "container_ip" $args }}
        {{- $ip := $args.ip }}
        {{- $args := dict "container" $container }}
        {{- template "container_port" $args }}
        {{- $port := $args.port }}
        {{- if $ip }}
            {{- $servers = add1 $servers }}
    server {{ $ip }}:{{ $port }};
        {{- end }}

  {{ end }}
{{ end }}
    {{- /* nginx-proxy/nginx-proxy#1105 */}}
    {{- if lt $servers 1 }}
    # Fallback entry
    server 127.0.0.1 down;
    {{- end }}
}

# Use X-Forwarded-Proto if available when behind a proxy
map $http_x_forwarded_proto $http_scheme {
    default $scheme;
    https "https";
    http "http";
}

server {
  gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript;

  server_name {{ $host }};
  proxy_buffering off;
  error_log /proc/self/fd/2;
  access_log /proc/self/fd/1;

  location / {
    proxy_pass http://{{ trim $host }};
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $http_scheme;
    proxy_set_header Connection $http_connection;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Sec-WebSocket-Extensions $http_sec_websocket_extensions;
    proxy_set_header Sec-WebSocket-Key $http_sec_websocket_key;
    proxy_set_header Sec-WebSocket-Version $http_sec_websocket_version;

    # HTTP 1.1 support
    proxy_http_version 1.1;
    proxy_set_header Connection "";
    client_body_buffer_size 100M;
    client_max_body_size 100M;
  }
}
{{ end }}
