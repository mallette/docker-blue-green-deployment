# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.2.0](https://gitlab.cemea.org/mallette/docker-blue-green-deployment/compare/v0.1.0...v0.2.0) (2024-10-24)

### Summary

Various fixes which improves reliability when some container is KO.
Various additional configuration to allow better proxy with more headers and websockets.
Add more configuration options for deployment process.

### Features
- 04afb50 feat: Manage websockets header
- ee22b62 feat: Increase Nginx allowed body size to 100MB
- 458ed67 feat: Precise number of instances for demo app
- 66d3fcd feat: Allow to precise single instances to be run
- 2cf4401 feat: Allow to customize healthcheck's timeout
- 4274834 feat: Manage HTTP scheme when behind proxy

### Bug Fixes
- 49af189 fix(nginx): Change template to have fallback in upstream
- cf229b7 fix: Add proxy restart
- ed606ec fix: Change healthcheck timeout variable name for consistency
