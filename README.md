# Blue-Green deployment with Docker Compose

Ceci est un projet de démonstration de déploiement avec la stratégie Blue-Green avec Docker-compose.

Le projet utilise docker-gen afin de générer la liste des conteneurs du réseau et l'ajouter à la configuration `upstream` de Nginx.

Nginx a été privilégié à Traefik car ce dernier n'est pas capable de drainer une requête existante alors que Nginx le gère nativement avec `nginx reload`.
Le drain d'une requête permet d'éviter son interruption en cas de changement de configuration Nginx

Pour tester le projet :

1. On crée des images de conteneurs pour tester la bascule

```
cd app && docker build -t v1 .
# Modifier quelque chose dans le index.html
cd app && docker build -t v2 .
```
2. On lance l'application v1

```
DOCKER_IMAGE_TAG=v1 make deploy
```

3. On surveille les retours dans une autre fenêtre

```
make curl
```

4. On lance l'application v2

```
DOCKER_IMAGE_TAG=v2 make deploy
```

##  Implémentation

Pour implémenter le déploiement blue-green dans un projet :

1. Copier le dossier deploy/ dans le projet
2. Ajouter dans le Makefile du projet à la racine les commandes suivantes (éventuellement à adapter) :

```
DOCKER_IMAGE_TAG ?= 'latest'

.PHONY: pull_app
pull_app:## Pull app's Docker images. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} docker compose pull

.PHONY: run_app_stack
run_app_stack:## Run app from scratch. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up -d

.PHONY: run_app_besides
run_app_besides: ## Run new container besides running one. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up -d --no-deps $(SCALE_COMMAND) --no-recreate

.PHONY: run_app_standalone
run_app_standalone: ## Run new container as standalone. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up $(CONTAINER_NAME) -d
```

3. Ajouter les variables d'environnement à chaque service dans le docker-compose.yml :

```
environment:
  - DEPLOYMENT_COLOR=${DEPLOYMENT_COLOR:-blue} # Pour tous les services à déployer
  - VIRTUAL_HOST=mondomaine.example # Pour les services qui reçoivent le trafic depuis le proxy
```

4. Ajouter le réseau de l'équilibreur de charge dans le docker-compose.yml :

```
  app:
    image: mon_service:${DOCKER_IMAGE_TAG}
    networks:
      - load_balancer_net

networks:
  load_balancer_net:
    name: load_balancer_net
    external: true
```

5. Facultatif : ajouter un healthckeck pour chaque service dans le docker-compose.yml :
```
  app:
    image: mon_service:${DOCKER_IMAGE_TAG}
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost"]
      interval: 3s
      timeout: 10s
      retries: 3
```

6. Enfin, utiliser la commande en précisant dans le bon ordre tous les conteneurs à déployer dans cette stratégie :

```
.PHONY: deploy
deploy: ## Run Blue-Green deployment script
	bash deploy/blue-green_deploy.sh app=2 another_app=2
```

## Usage du script

```
# bash deploy/blue-green_deploy.sh -h
Usage: blue-green_deploy.sh [-h] [--no-maintenance] [CONTAINERS]=[SCALE]...
Deploy the list of containers according to the blue-green deployment strategy.
The script deploys each container one per one in the order of the CONTAINERS list.
SCALE, the number of instances of the same app which can run simultaneously, can be either 1 or 2.
By default, a maintenance page is always displayed before switching apps.

    -h                display this help and exit
    --no-maintenance  disable maintenance when switching containers
```

### Instances uniques

:warning: Tout les conteneurs ne peuvent pas être déployés en plusieurs exemplaires.
Les conteneurs de base de données par exemple risque de corrompre la donnée si plusieurs instances tournent simultanément avec le même volume.
Pour éviter cette corruption de données, il est nécessaire d'indiquer au script de déploiement que seule une instance doit être déployée.

Exemple :

```
bash deploy/blue-green_deploy.sh database=1 app=2
```

Pour que cette unique instance puisse être accessible par les conteneurs des autres couleurs, cette instance doit être connectée aux réseaux des deux couleurs simultanément.

Exemple de `docker-compose.yml` :
```
  database:
    image: postgresql
    volumes:
      - database_data:/var/lib/postgresql
    networks:
      - internal_color_blue
      - internal_color_green
```

### Options

Il est possible d'ajouter ces variables d'environnement au moment du lancement du script bash
pour changer le comportement:


```
DEPLOYMENT_LB_LISTEN_PORT=80 # Spécifie le port d'écoute du load balancer
DEPLOYMENT_HEALTHCHECK_TIMEOUT='10 seconds' # Spécifie le timeout pendant lequel attendre que le healthcheck réussit pour pouvoir continuer le déploiement
```

## License

[AGPL 3.0](LICENSE)
