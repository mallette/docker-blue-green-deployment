DOCKER_IMAGE_TAG ?= 'latest'

.PHONY: deploy
deploy: ## Run Blue-Green deployment script
	bash deploy/blue-green_deploy.sh app=2 another_app=2

.PHONY: pull_app
pull_app:## Pull app's Docker images. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} docker compose pull --ignore-buildable

.PHONY: run_app_stack
run_app_stack:## Run app from scratch. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up -d

.PHONY: run_app_besides
run_app_besides: ## Run new container besides running one. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up -d --no-deps $(SCALE_COMMAND) --no-recreate

.PHONY: run_app_standalone
run_app_standalone: ## Run new container as standalone. Required by blue-green_deploy.sh.
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} DEPLOYMENT_COLOR=$(DEPLOYMENT_COLOR) docker compose up $(CONTAINER_NAME) -d

.PHONY: curl
curl:
	while true; do \
	curl localhost:80 || true; \
	sleep 0.1; \
	done

.PHONY: stop
stop: ## Stop all containers and load balancer
	DOCKER_IMAGE_TAG=${DOCKER_IMAGE_TAG} docker compose down
	cd deploy && docker compose -f docker-compose.load_balancer.yml down

.PHONY: changelog
changelog: ## Display changelog since last merge
	@echo "Features"
	@git log --oneline --no-merges --grep "^feat" $$(git describe --tags --abbrev=0)..HEAD
	@echo "Bug Fixes"
	@git log --oneline --no-merges --grep "^fix" $$(git describe --tags --abbrev=0)..HEAD
